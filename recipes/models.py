from django.db import models

# Create your models here.
class Recipe(models.Model):
    title = models.CharField(max_length=100)
    description = models.TextField()
    picture = models.URLField(max_length=300)
    created_on = models.DateTimeField(auto_now_add=True)
